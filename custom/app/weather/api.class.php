<?php
import('Custom.App.Weather.weatherMode');
class api extends Activity {
    /**
     * @var weatherMode
     */
    protected $wm;
    protected function onStart() {
        $this->wm=weatherMode::init();
    }

    function indexTask(){
        header('HTTP/1.1 404 Not Found');
        header("status: 404 Not Found");
    }
    function weatherTask(){
        $type=isset($_GET['type'])?$_GET['type']:weatherMode::TYPE_FUTURE;
        $city=isset($_GET['city'])?$_GET['city']:"徐州";
        switch($type){
            case weatherMode::TYPE_UNI:
                $data=$this->wm->getUni();
                break;
            case weatherMode::TYPE_SK:
                $data=$this->wm->getSk($city);
                break;
            case weatherMode::TYPE_TIPS:
                $data=$this->wm->getTips($city);
                break;
            case weatherMode::TYPE_3H:
                $data=$this->wm->get3h($city);
                break;
            case weatherMode::TYPE_FUTURE:
            default:
                $data=$this->wm->getFuture($city);
                break;
        }
        if(is_array($data)){
            $result['data']=$data;
            $result['resultCode']=200;
        }else{
            $result['resultCode']=500;
            $result['resultMessage']=$data;
        }
        View::displayAsJson($result);
    }
}
