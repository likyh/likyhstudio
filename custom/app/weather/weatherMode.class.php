<?php
import('Lib.Cache.SimpleMemcache');
import('Plugin.Juhe.JuheWeatherMode');
class weatherMode extends Data {
    CONST TYPE_SK='sk';
    CONST TYPE_TIPS='tips';
    CONST TYPE_FUTURE='future';
    CONST TYPE_UNI='uni';
    CONST TYPE_3H='3h';
    /**
     * @var SimpleMemcache
     */
    protected $mmc;
    /**
     * @var JuheWeatherMode
     */
    protected $jwm;
    protected function onStart(){
        $this->mmc= new SimpleMemcache("weather");
        $this->jwm=JuheWeatherMode::init();
    }

    /**
     * @param string $city 要查询的城市
     * @param $returnType
     * @return bool
     */
    protected function updateBase($city, $returnType){
        $data=$this->jwm->getBase($city);
        if($data['resultcode']!=200){
            return $data['reason'];
        }
        //实时天气保存10min
        $sk=$data['result']['sk'];
        $sk['time']=strtotime($sk['time']);
        $this->mmc->set("sk".$city,$sk,600);

        // 天气提示保存到当天午夜
        $tips['dressing']=$data['result']['today']["dressing_index"];
        $tips['dressing_advice']=$data['result']['today']["dressing_advice"];
        $tips['uv']=$data['result']['today']["uv_index"];
        $tips['comfort']=$data['result']['today']["comfort_index"];
        $tips['wash']=$data['result']['today']["wash_index"];
        $tips['travel']=$data['result']['today']["travel_index"];
        $tips['exercise']=$data['result']['today']["exercise_index"];
        $tips['drying']=$data['result']['today']["drying_index"];
        $tips['date']=date("Y-m-d");
        $tips['time']=time();
        $this->mmc->set("tips".$city,$tips,strtotime("today 23:59"));

        // 天气预报保存到当天午夜
        $future=$data['result']['future'];
        foreach($future as $k=>$v){
            $t=strtotime($future[$k]['date']);
            $future[$k]['date']=date("Y-m-d",$t);
            $future[$k]['time']=$t;
        }
        $this->mmc->set("future".$city,$future,strtotime("today 23:59"));

        // 返回'参数中的类型'
        $returnType=isset($$returnType)?$$returnType:true;
        return $returnType;
    }

    protected function updateSk($city){
        return $this->updateBase($city,'sk');
    }
    protected function updateTips($city){
        return $this->updateBase($city,'tips');
    }
    protected function updateFuture($city){
        return $this->updateBase($city,'future');
    }

    /**
     * 更新weather id代表的含义
     * @return array|null
     */
    protected function updateUni(){
        $data=$this->jwm->getUni();
        if($data['resultcode']!=200){
            return $data['reason'];
        }

        $uni=array();
        foreach($data['result'] as $v){
            $uni[$v['wid']]=$v['weather'];
        }
        $this->mmc->set("uni", $uni);
        return $uni;
    }

    /**
     * 更新3小时天气预报
     * @param $city
     * @return array
     */
    protected function update3h($city){
        $data=$this->jwm->getForecast3h($city);
        if($data['resultcode']!=200){
            return $data['reason'];
        }
        $forecast=array();
        foreach($data['result'] as $v){
            $startTime=date("YmdHis",strtotime($v['sfdate']));
            $forecast[$startTime]['weatherid']=$v['weatherid'];
            $forecast[$startTime]['temp1']=$v['temp1'];
            $forecast[$startTime]['temp2']=$v['temp2'];
            $forecast[$startTime]['startTime']=strtotime($v['sfdate']);
            $forecast[$startTime]['endTime']=strtotime($v['efdate']);
        }
        $this->mmc->set("3h".$city, $forecast, 10800);
        return $forecast;
    }

    public function getFuture($city){
        return $this->mmc->get("future".$city)?:$this->updateFuture($city);
    }
    public function getSk($city){
        return $this->mmc->get("sk".$city)?:$this->updateSk($city);
    }
    public function getUni(){
        return $this->mmc->get("uni")?:$this->updateUni();
    }
    public function getTips($city){
        return $this->mmc->get("tips".$city)?:$this->updateTips($city);
    }
    public function get3h($city){
        return $this->mmc->get("3h".$city)?:$this->update3h($city);
    }
}